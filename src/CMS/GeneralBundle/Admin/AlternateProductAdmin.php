<?php

namespace CMS\GeneralBundle\Admin;

use CMS\BaseBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AlternateProductAdmin extends BaseAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('productTitle')
            ->add('companyName')

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('country')
            ->add('productTitle')
            ->add('companyName')
            ->add('created')
            ->add('updated')

            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('productImage')
            ->add('productImage', 'sonata_type_model_list',  array('label' => 'Image(Max Image Size:2MB, Size:  X )'),
                array('link_parameters' => array( 'provider' => 'sonata.media.provider.image')))
            ->add('productTitle')
            ->add('country', 'sonata_type_model_autocomplete', array('property'=>'countryname'))
            ->add('companyName')
            ->add('suggestions')
            ->add('productSeverity')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('productImage')
            ->add('productTitle')
            ->add('country')
            ->add('companyName')
            ->add('suggestions')
            ->add('productSeverity')
            ->add('created')
            ->add('created_by')
            ->add('updated')
            ->add('updated_by')
        ;
    }
}
