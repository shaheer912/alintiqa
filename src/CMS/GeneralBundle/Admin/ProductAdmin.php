<?php

namespace CMS\GeneralBundle\Admin;

use CMS\BaseBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductAdmin extends BaseAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('companyName')
            ->add('websiteUrl')
            ->add('enabled')

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('country')
            ->add('title')
            ->add('companyName')
            ->add('websiteUrl')
            ->add('enabled')
            ->add('created')
            ->add('updated')

            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $query = $this->getQBForAlternateProducts($this->id($this->getSubject()));
        //var_dump($query);

        $formMapper
            //->add('productImage')

            ->add('country', 'sonata_type_model_autocomplete', array('property'=>'countryname'))
            ->add('companyName')
            ->add('websiteUrl')
            ->add('productImage', 'sonata_type_model_list',  array('label' => 'Image(Max Image Size:2MB, Size:  X )'),
                array('link_parameters' => array( 'provider' => 'sonata.media.provider.image')))
            ->add('title')
            ->add('suggestions')
            ->add('productSeverity')
//            ->add('alternate_products', 'sonata_type_collection', array(
//                'type_options' => array(
//                    // Prevents the "Delete" option from being displayed
//                    'delete' => false,
//                    'delete_options' => array(
//                        // You may otherwise choose to put the field but hide it
//                        'type'         => 'hidden',
//                        // In that case, you need to fill in the options as well
//                        'type_options' => array(
//                            'mapped'   => false,
//                            'required' => false,
//                        )
//                    )
//                )
//            ), array(
//                'edit' => 'inline',
//                'inline' => 'table',
//                'sortable' => 'position',
//                //'admin_code' => 'cms_general.admin.alternate_product',
//            ))
            ->add('alternate_products', null, array(
                'required' => true,
                //'query' => $query,
                //'class'    => 'CMS\GeneralBundle\Entity\Product',
                'choices' => $query->getQuery()->getResult()
            ))
            ->add('enabled')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('productImage')
            ->add('title')
            ->add('country')
            ->add('companyName')
            ->add('websiteUrl')
            ->add('suggestions')
            ->add('productSeverity')
            ->add('alternate_products')
            ->add('enabled')
            ->add('created')
            ->add('created_by')
            ->add('updated')
            ->add('updated_by')
        ;
    }
    protected function getQBForAlternateProducts( $current_id ) {
        $em = $this->modelManager->getEntityManager('CMSGeneralBundle:Product');
        /** @var \Doctrine\ORM\QueryBuilder $query */
        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('CMSGeneralBundle:Product', 'c')
            ->where('c.enabled = true')
            ->orderBy('c.title', 'DESC');
        if( $current_id ) {
            $query->andWhere("c.id != $current_id ");
        }
        return $query;
    }
}
