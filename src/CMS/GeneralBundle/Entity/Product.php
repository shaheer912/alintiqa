<?php

namespace CMS\GeneralBundle\Entity;
use JsonSerializable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use CMS\BaseBundle\Entity\Base;

/**
 * Product
 *
 * @ORM\Table(name="cms_product")
 * @ORM\Entity(repositoryClass="CMS\GeneralBundle\Repository\ProductRepository")
 * @UniqueEntity( fields={"title"}, message="Product with this name already exist!")
 */
class Product extends Base implements JsonSerializable {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^.*\.(jpg|jpeg|gif|JPG|png|PNG)$/", message="Not valid image, please select valid image.")
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="product_image_id", referencedColumnName="id")
     */
    protected $productImage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="product_website", type="string", length=255, nullable=true)
     */
    private $websiteUrl;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="CMS\GeneralBundle\Entity\Country", inversedBy="country")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     **/
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="suggestions", type="text")
     */
    private $suggestions;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="CMS\GeneralBundle\Entity\ProductSeverity", inversedBy="productSeverity")
     * @ORM\JoinColumn(name="product_severity_id", referencedColumnName="id", nullable=false)
     **/
    private $productSeverity;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="alternate_products", cascade={"persist"})
     * @ORM\JoinTable(name="alternate_products",
     *     joinColumns={
     *     @ORM\JoinColumn(name="alternate_product_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="original_product_id", referencedColumnName="id")
     *   }
     * )
     */
    private $alternate_products;

    /**
     * @var string
     *
     */
    private $countryCode;

    /**
     * @var string
     */
    private $productSeverityCode;

    /**
     * @var string
     */
    private $image_key;

    /**
     * @var string
     */
    private $firebase_key;

    function __construct()
    {
        $this->alternate_products = new ArrayCollection();
    }

    public function loadFromArray( $key, $array )
    {
        $this->setTitle($array['title']);
        $this->setProductImage($array['image']);
        $this->setWebsiteUrl($array['website_url']);
        $this->setCountryCode($array['country']);
        $this->setCompanyName($array['company']);
        $this->setSuggestions($array['suggestions']);
        $this->setProductSeverityCode($array['productSeverity']);
        $this->setImageKey($array['imageKey']);
        $this->setFirebaseKey($key);
        return $this;
    }

    public function loadFromJson( $key, $json )
    {
        $decoded = json_decode($json);
        if( isset($decoded->title) ) {
            $this->setTitle($decoded->title);
        }
        if( isset($decoded->website_url) ) {
            $this->setWebsiteUrl($decoded->website_url);
        }
        if( isset($decoded->country) ) {
            $this->setCountryCode($decoded->country);
        }
        if( isset($decoded->company) ) {
            $this->setCompanyName($decoded->company);
        }
        if( isset($decoded->suggestions) ) {
            $this->setSuggestions($decoded->suggestions);
        }
        if( isset($decoded->productSeverity) ) {
            $this->setProductSeverityCode($decoded->productSeverity);
        }
        if( isset($decoded->imageKey) ) {
            $this->setImageKey($decoded->imageKey);
        }
        $this->setFirebaseKey($key);
//        $this->setTitle($decoded->title);
//        //$this->setProductImage($decoded->image);
//        $this->setWebsiteUrl($decoded->website_url);
//        $this->setCountryCode($decoded->country);
//        $this->setCompanyName($decoded->company);
//        $this->setSuggestions($decoded->suggestions);
//        $this->setProductSeverityCode($decoded->productSeverity);
//        $this->setImageKey($decoded->imageKey);
//        $this->setFirebaseKey($key);

//        $alternate_products_arr = array();
//        $alternate_products = $this->getAlternateProducts();
//        //dump($alternate_products);
//        foreach( $alternate_products as $alternate_product ) {
//            /** @var Product $alternate_product */
////            $alternate_product_el = $alternate_product->jsonSerialize();
//            $alternate_products_arr[] = $alternate_product->getId();
//        }
//
//        $return = array(
//            'id'    => $this->getId(),
//            'title' => $this->getTitle(),
//            'image' => $this->getProductImage()->getId(),
//            'website_url' => $this->getWebsiteUrl(),
//            'country' => $this->getCountry()->getCountryname(),
//            'company' => $this->getCompanyName(),
//            'suggestions' => $this->getSuggestions(),
//            'productSeverity' => $this->getProductSeverity()->getCode(),
//            'alternate_products' => $alternate_products_arr,
//        );

        return $this;
    }

    public function jsonSerialize()
    {
        $alternate_products_arr = array();
        $alternate_products = $this->getAlternateProducts();
        //dump($alternate_products);
        foreach( $alternate_products as $alternate_product ) {
            /** @var Product $alternate_product */
//            $alternate_product_el = $alternate_product->jsonSerialize();
            $alternate_products_arr[] = $alternate_product->getId();
        }

        $return = array(
            'id'    => $this->getId(),
            'title' => $this->getTitle(),
            'image' => $this->getProductImage()->getId(),
            'website_url' => $this->getWebsiteUrl(),
            'country' => $this->getCountry()->getCountryname(),
            'company' => $this->getCompanyName(),
            'suggestions' => $this->getSuggestions(),
            'productSeverity' => $this->getProductSeverity()->getCode(),
            'alternate_products' => $alternate_products_arr,
            'imageKey' => $this->getImageKey(),
        );

        return $return;
    }

    static function getProductSeverityCodes() {
        return array(
            'redzone' => 'Red Zone',
            'normal' => 'Normal',
        );
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productImage.
     *
     * @param string|null $productImage
     *
     * @return Product
     */
    public function setProductImage($productImage = null)
    {
        $this->productImage = $productImage;

        return $this;
    }

    /**
     * Get productImage.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getProductImage()
    {
        return $this->productImage;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set websiteUrl.
     *
     * @param string $websiteUrl
     *
     * @return Product
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl.
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }
    /**
     * Set country.
     *
     * @param \CMS\GeneralBundle\Entity\Country $country
     *
     * @return City
     */
    public function setCountry( \CMS\GeneralBundle\Entity\Country $country ) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \CMS\GeneralBundle\Entity\Country
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set companyName.
     *
     * @param string $companyName
     *
     * @return Product
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set countryCode.
     *
     * @param string $countryCode
     *
     * @return Product
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode.
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set suggestions.
     *
     * @param string $suggestions
     *
     * @return Product
     */
    public function setSuggestions($suggestions)
    {
        $this->suggestions = $suggestions;

        return $this;
    }

    /**
     * Get suggestions.
     *
     * @return string
     */
    public function getSuggestions()
    {
        return $this->suggestions;
    }

    /**
     * Set productSeverity.
     *
     * @param ProductSeverity $productSeverity
     *
     * @return Product
     */
    public function setProductSeverity($productSeverity)
    {
        $this->productSeverity = $productSeverity;

        return $this;
    }

    /**
     * Get productSeverity.
     *
     * @return ProductSeverity
     */
    public function getProductSeverity()
    {
        return $this->productSeverity;
    }

    /**
     * Adds an Alternate Product
     *
     * @param Product $alternate_product
     *
     * @return Product
     */
    public function addAlternateProduct(Product $alternate_product) {
        $this->alternate_products->add($alternate_product);

        return $this;
    }

    /**
     * Removes an Alternate Product
     *
     * @param Product $alternate_product
     *
     * @return Product
     */
    public function removeAlternateProduct(Product $alternate_product) {
        $this->alternate_products->removeElement($alternate_product);

        return $this;
    }

    public function getAlternateProducts() {
        return $this->alternate_products;
    }

    /**
     * Set Product Severity Code.
     *
     * @param string $productSeverityCode
     *
     * @return Product
     */
    public function setProductSeverityCode($productSeverityCode)
    {
        $this->productSeverityCode = $productSeverityCode;

        return $this;
    }

    /**
     * Get Product Severity Code.
     *
     * @return string
     */
    public function getProductSeverityCode()
    {
        return $this->productSeverityCode;
    }

    /**
     * Set Image Key
     *
     * @param string $imageKey
     *
     * @return Product
     */
    public function setImageKey($imageKey)
    {
        $this->image_key = $imageKey;

        return $this;
    }

    /**
     * Get Image Key
     *
     * @return string
     */
    public function getImageKey()
    {
        return $this->image_key;
    }
    /**
     * Set Firebase key.
     *
     * @param string $firebase_key
     *
     * @return Product
     */
    public function setFirebaseKey($firebase_key)
    {
        $this->firebase_key = $firebase_key;

        return $this;
    }

    /**
     * Get Firebase key.
     *
     * @return string
     */
    public function getFirebaseKey()
    {
        return $this->firebase_key;
    }

}
