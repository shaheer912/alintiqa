<?php

namespace CMS\GeneralBundle\Entity;

use CMS\BaseBundle\Entity\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Homeslider
 *
 * @ORM\Table(name="cms_homeslider")
 * @ORM\Entity(repositoryClass="CMS\GeneralBundle\Repository\HomesliderRepository")
 */
class Homeslider extends Base
{
    /**
      * @var int
      *
      * @ORM\Column(name="id", type="integer")
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="AUTO")
      */
     protected $id;

     /**
      * @var string
      *
      * @ORM\Column(name="title", type="string", length=255)
      */
     protected $title;

     /**
      * @Assert\NotBlank()
      * @Assert\Regex(pattern="/^.*\.(jpg|jpeg|gif|JPG|png|PNG)$/", message="Not valid image, please select valid image.")
      * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
      * @ORM\JoinColumn(name="image", referencedColumnName="id")
      */
     protected $image;

     /**
      * @var string
      *
      * @ORM\Column(name="description", type="text")
      */
     protected $description;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Homeslider
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Homeslider
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Homeslider
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
