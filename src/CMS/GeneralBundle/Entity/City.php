<?php

namespace CMS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use CMS\BaseBundle\Entity\Base;

/**
 * City
 *
 * @ORM\Table(name="cms_cities")
 * @ORM\Entity
 * @UniqueEntity( fields={"cityname"}, message="City with this name already exist!")
 */
class City extends Base {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @Assert\NotBlank()
	 * @ORM\ManyToOne(targetEntity="CMS\GeneralBundle\Entity\Country", inversedBy="country")
	 * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
	 **/
	protected $country;

	/**
	 * @Assert\NotBlank()
	 * @var string
	 *
	 * @ORM\Column(name="cityname", type="string", length=255)
	 */
	protected $cityname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="citycode", type="string", length=255, nullable=true)
	 */
	protected $citycode;

	/**
	 * @ORM\OneToOne(targetEntity="\CMS\FrontUserBundle\Entity\FrontUser", mappedBy="city",cascade={"persist"})
	 */
	protected $frontuser;

	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set cityname.
	 *
	 * @param string $cityname
	 *
	 * @return City
	 */
	public function setCityname( $cityname ) {
		$this->cityname = $cityname;

		return $this;
	}

	/**
	 * Get cityname.
	 *
	 * @return string
	 */
	public function getCityname() {
		return $this->cityname;
	}

	/**
	 * Set citycode.
	 *
	 * @param string|null $citycode
	 *
	 * @return City
	 */
	public function setCitycode( $citycode = null ) {
		$this->citycode = $citycode;

		return $this;
	}

	/**
	 * Get citycode.
	 *
	 * @return string|null
	 */
	public function getCitycode() {
		return $this->citycode;
	}

	/**
	 * Set country.
	 *
	 * @param \CMS\GeneralBundle\Entity\Country $country
	 *
	 * @return City
	 */
	public function setCountry( \CMS\GeneralBundle\Entity\Country $country ) {
		$this->country = $country;

		return $this;
	}

	/**
	 * Get country.
	 *
	 * @return \CMS\GeneralBundle\Entity\Country
	 */
	public function getCountry() {
		return $this->country;
	}
}
