<?php

namespace CMS\GeneralBundle\Entity;

use CMS\BaseBundle\Entity\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 *
 * @ORM\Table(name="cms_company")
 * @ORM\Entity(repositoryClass="CMS\GeneralBundle\Repository\CompanyRepository")
 */
class Company extends Base {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	protected $name;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="registered_name", type="string", length=255)
	 */
	protected $registeredname;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Regex(pattern="/^.*\.(jpg|jpeg|gif|JPG|png|PNG)$/", message="Not valid image, please select valid image.")
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
	 * @ORM\JoinColumn(name="logo", referencedColumnName="id")
	 */
	protected $logo;
	/**
	 * @ORM\OneToOne(targetEntity="\CMS\FrontUserBundle\Entity\FrontUser", mappedBy="company",cascade={"persist"})
	 */
	protected $frontuser;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Company
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set frontuser.
	 *
	 * @param \CMS\FrontUserBundle\Entity\FrontUser|null $frontuser
	 *
	 * @return Company
	 */
	public function setFrontuser( \CMS\FrontUserBundle\Entity\FrontUser $frontuser = null ) {
		$this->frontuser = $frontuser;

		return $this;
	}

	/**
	 * Get frontuser.
	 *
	 * @return \CMS\FrontUserBundle\Entity\FrontUser|null
	 */
	public function getFrontuser() {
		return $this->frontuser;
	}
}
