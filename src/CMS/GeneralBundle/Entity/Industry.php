<?php

namespace CMS\GeneralBundle\Entity;

use CMS\BaseBundle\Entity\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Industry
 *
 * @ORM\Table(name="cms_industry")
 * @ORM\Entity(repositoryClass="CMS\GeneralBundle\Repository\IndustryRepository")
 */
class Industry extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

	/**
	 * @ORM\OneToOne(targetEntity="\CMS\FrontUserBundle\Entity\FrontUser", mappedBy="industry",cascade={"persist"})
	 */
	protected $frontuser;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Industry
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
