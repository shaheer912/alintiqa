<?php

namespace CMS\FrontUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT p FROM CMSGeneralBundle:Product p";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('CMSFrontUserBundle:Default:index.html.twig', array('pagination' => $pagination));
    }

    /**
     * @Route("/product/{id}", name="cms_product_detail")
     */
    public function product(Request $request, $id = null)
    {
        $repo = $this->getDoctrine()->getRepository('CMSGeneralBundle:Product');
        $product = $repo->find($id);

        return $this->render('CMSFrontUserBundle:Default:product.html.twig', array('product' => $product));
    }

    /**
     * @Route("/firebase/", name="firebase")
     */
    public function firebase() {

        $repo = $this->getDoctrine()->getRepository('CMSGeneralBundle:Product');
        /** @var \CMS\GeneralBundle\Entity\Product $product */
        $product = $repo->find(2);

        /** @var \Kreait\Firebase $firebase */

        $reference = 'products';

        $firebase = $this->container->get('firebase');
        $products = $firebase->getDatabase()->getReference($reference);
        $products->remove();

        $product_json = json_encode($product);

        $saved = $products->push(
            $product_json
        );

        $sn = $products->getSnapshot();
        var_dump($sn->getValue());
//        var_dump($products->getChildKeys());

        return new Response();
    }
}
