<?php

namespace CMS\FrontUserBundle\Admin;

use CMS\BaseBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class FrontUserAdmin extends BaseAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('designation')
            ->add('company')
            ->add('businessemail')
            ->add('companywebsite')
            ->add('department')
            ->add('industry')
            ->add('companyalias')
            ->add('companysize')
            ->add('hqaddress')
            ->add('locationifdifferentfromhq')
            ->add('createAt')
            ->add('updateAt')
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            
            ->add('name')
            ->add('designation')
            ->add('company')
            ->add('businessemail')
            ->add('enabled')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('designation')
            ->add('company')
            ->add('businessemail')
            ->add('companywebsite')
            ->add('department')
            ->add('industry')
            ->add('companyalias')
            ->add('companysize')
            ->add('hqaddress')
            ->add('locationifdifferentfromhq')
            ->add('enabled')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            
            ->add('name')
            ->add('designation')
            ->add('company')
            ->add('businessemail')
            ->add('companywebsite')
            ->add('department')
            ->add('industry')
            ->add('companyalias')
            ->add('companysize')
            ->add('hqaddress')
            ->add('locationifdifferentfromhq')
            ->add('createAt')
            ->add('updateAt')
            ->add('enabled')
        ;
    }
}
