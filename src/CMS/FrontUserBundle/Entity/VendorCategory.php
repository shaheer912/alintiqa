<?php

namespace CMS\FrontUserBundle\Entity;

use CMS\BaseBundle\Entity\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * VendorCategory
 *
 * @ORM\Table(name="cms_vendor_category")
 * @ORM\Entity(repositoryClass="CMS\FrontUserBundle\Repository\VendorCategoryRepository")
 */
class VendorCategory extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return VendorCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
