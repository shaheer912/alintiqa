<?php

namespace CMS\FrontUserBundle\Entity;

use CMS\BaseBundle\Entity\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FrontUser
 *
 * @ORM\Table(name="cms_front_user")
 * @ORM\Entity(repositoryClass="CMS\FrontUserBundle\Repository\FrontUserRepository")
 */
class FrontUser extends Base {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	protected $name;

	/**
	 * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\Group")
	 * @ORM\JoinTable(name="cms_front_user_group",
	 *      joinColumns={@ORM\JoinColumn(name="front_user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
	 * )
	 */
	protected $groups;

	/**
	 * @ORM\ManyToMany(targetEntity="\CMS\FrontUserBundle\Entity\VendorCategory")
	 * @ORM\JoinTable(name="cms_front_user_vendors",
	 *      joinColumns={@ORM\JoinColumn(name="front_user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
	 * )
	 */
	protected $vendorcategory;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="designation", type="string", length=255)
	 */
	protected $designation;

	/**
	 * @Assert\NotBlank()
	 * @ORM\OneToOne(targetEntity="\CMS\GeneralBundle\Entity\Company" , inversedBy="frontuser" ,cascade={"persist"})
	 * @ORM\JoinColumn(name="company", referencedColumnName="id")
	 */
	protected $company;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="businessemail", type="string", length=255)
	 */
	protected $businessemail;

	/**
	 * @Assert\NotBlank()
	 * @ORM\OneToOne(targetEntity="\CMS\GeneralBundle\Entity\City" , inversedBy="frontuser" ,cascade={"persist"})
	 * @ORM\JoinColumn(name="city", referencedColumnName="id")
	 */
	protected $city;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="companywebsite", type="string", length=255)
	 */
	protected $companywebsite;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="department", type="string", length=255)
	 */
	protected $department;

	/**
	 * @Assert\NotBlank()
	 * @ORM\OneToOne(targetEntity="\CMS\GeneralBundle\Entity\Industry" , inversedBy="frontuser" ,cascade={"persist"})
	 * @ORM\JoinColumn(name="industry", referencedColumnName="id")
	 */
	protected $industry;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="companyalias", type="string", length=255)
	 */
	protected $companyalias;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="companysize", type="string", length=255)
	 */
	protected $companysize;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="hqaddress", type="string", length=255)
	 */
	protected $hqaddress;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @ORM\Column(name="locationifdifferentfromhq", type="string", length=255)
	 */
	protected $locationifdifferentfromhq;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return FrontUser
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set designation.
	 *
	 * @param string $designation
	 *
	 * @return FrontUser
	 */
	public function setDesignation( $designation ) {
		$this->designation = $designation;

		return $this;
	}

	/**
	 * Get designation.
	 *
	 * @return string
	 */
	public function getDesignation() {
		return $this->designation;
	}

	/**
	 * Set businessemail.
	 *
	 * @param string $businessemail
	 *
	 * @return FrontUser
	 */
	public function setBusinessemail( $businessemail ) {
		$this->businessemail = $businessemail;

		return $this;
	}

	/**
	 * Get businessemail.
	 *
	 * @return string
	 */
	public function getBusinessemail() {
		return $this->businessemail;
	}

	/**
	 * Set companywebsite.
	 *
	 * @param string $companywebsite
	 *
	 * @return FrontUser
	 */
	public function setCompanywebsite( $companywebsite ) {
		$this->companywebsite = $companywebsite;

		return $this;
	}

	/**
	 * Get companywebsite.
	 *
	 * @return string
	 */
	public function getCompanywebsite() {
		return $this->companywebsite;
	}

	/**
	 * Set department.
	 *
	 * @param string $department
	 *
	 * @return FrontUser
	 */
	public function setDepartment( $department ) {
		$this->department = $department;

		return $this;
	}

	/**
	 * Get department.
	 *
	 * @return string
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * Set industry.
	 *
	 * @param string $industry
	 *
	 * @return FrontUser
	 */
	public function setIndustry( $industry ) {
		$this->industry = $industry;

		return $this;
	}

	/**
	 * Get industry.
	 *
	 * @return string
	 */
	public function getIndustry() {
		return $this->industry;
	}

	/**
	 * Set companyalias.
	 *
	 * @param string $companyalias
	 *
	 * @return FrontUser
	 */
	public function setCompanyalias( $companyalias ) {
		$this->companyalias = $companyalias;

		return $this;
	}

	/**
	 * Get companyalias.
	 *
	 * @return string
	 */
	public function getCompanyalias() {
		return $this->companyalias;
	}

	/**
	 * Set companysize.
	 *
	 * @param string $companysize
	 *
	 * @return FrontUser
	 */
	public function setCompanysize( $companysize ) {
		$this->companysize = $companysize;

		return $this;
	}

	/**
	 * Get companysize.
	 *
	 * @return string
	 */
	public function getCompanysize() {
		return $this->companysize;
	}

	/**
	 * Set hqaddress.
	 *
	 * @param string $hqaddress
	 *
	 * @return FrontUser
	 */
	public function setHqaddress( $hqaddress ) {
		$this->hqaddress = $hqaddress;

		return $this;
	}

	/**
	 * Get hqaddress.
	 *
	 * @return string
	 */
	public function getHqaddress() {
		return $this->hqaddress;
	}

	/**
	 * Set locationifdifferentfromhq.
	 *
	 * @param string $locationifdifferentfromhq
	 *
	 * @return FrontUser
	 */
	public function setLocationifdifferentfromhq( $locationifdifferentfromhq ) {
		$this->locationifdifferentfromhq = $locationifdifferentfromhq;

		return $this;
	}

	/**
	 * Get locationifdifferentfromhq.
	 *
	 * @return string
	 */
	public function getLocationifdifferentfromhq() {
		return $this->locationifdifferentfromhq;
	}

	/**
	 * Set company.
	 *
	 * @param \CMS\GeneralBundle\Entity\Company|null $company
	 *
	 * @return FrontUser
	 */
	public function setCompany( \CMS\GeneralBundle\Entity\Company $company = null ) {
		$this->company = $company;

		return $this;
	}

	/**
	 * Get company.
	 *
	 * @return \CMS\GeneralBundle\Entity\Company|null
	 */
	public function getCompany() {
		return $this->company;
	}
}
