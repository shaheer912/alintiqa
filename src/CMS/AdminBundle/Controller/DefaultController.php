<?php

namespace CMS\AdminBundle\Controller;

use CMS\GeneralBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;

class DefaultController extends Controller
{

    /**
     * @Route("/fbadmin/", name="admin_index")
     */
    public function indexAction(Request $request) {
        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $session = $this->container->get('session');

        $config = array();

        if( !$session->has('logged_in') || $session->get('logged_in') === false ) {

            if( $request->isMethod('post') ) {
                if( $request->request->get('username') == 'admin' && $request->request->get('password') == 'admin' ) {
                    $session->set('logged_in', true);
                    return $this->redirectToRoute('admin_product_list');
                }
            }
            else {
                $config['error'] = 'Invalid Username or Password';
            }
        }

        return $this->render('CMSAdminBundle:Default:login.html.twig', $config);
    }

    /**
     * @Route("/fbadmin/logout", name="admin_logout")
     */
    public function logoutAction(Request $request) {
        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $session = $this->container->get('session');

        $config = array();

        $session->remove('logged_in');

        return $this->redirectToRoute('admin_index');
    }

    /**
     * @Route("/fbadmin/products", name="admin_product_list")
     */
    public function productListAction()
    {
        $reference = 'products';

        $firebase = $this->container->get('firebase');
        $products = $firebase->getDatabase()->getReference($reference);

        $data = array(
            'products' => $this->doMapping($products->getValue())
        );

        return $this->render('CMSAdminBundle:Default:index.html.twig', $data);
    }

    /**
     * @Route("/fbadmin/products/edit/{id}", name="admin_product_edit")
     */
    public function productEditAction(Request $request, $id)
    {
        $reference = 'products';

        /** @var \Kreait\Firebase $firebase */
        $firebase = $this->container->get('firebase');

        /** @var \Kreait\Firebase\Database\Reference $products */
        $products = $firebase->getDatabase()->getReference($reference);

        $product = $products->getChild($id);
        if( is_null($product->getValue()) ) {
            throw $this->createNotFoundException('Invalid Product ID');
        }

        $storage = $firebase->getStorage()->getStorageClient();
        $bucket = $storage->bucket('alintiqa-a2dcb.appspot.com');

        $saved = false;

        if( $request->isMethod('POST') ) {
            $fields = $request->request;

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $product_image */
            $product_image = $request->files->get('product_image');

            $orignName = $product_image->getClientOriginalName();
            $orignName = explode(".", $orignName);
            $ext = end($orignName);

            $filename = sha1(time() . $product_image->getClientOriginalName()) . ".$ext";

            $uploaded = $bucket->upload(
                file_get_contents($product_image->getRealPath()),
                [
                    'name' => $filename
                ]
            );

            $newProduct = $product->getValue();
            $newProduct = json_decode($newProduct);
            $newProduct->title = $fields->get('title');
            $newProduct->website_url = $fields->get('website_url');
            $newProduct->company = $fields->get('company');
            $newProduct->country = $fields->get('country');
            $newProduct->productSeverity = $fields->get('product_severity');
            $newProduct->suggestions = $fields->get('suggestions');
            $newProduct->imageKey = $filename;

            $update = [
                "$id" => json_encode($newProduct)
            ];

            $products->update($update);

            $product = $products->getChild($id);
            $saved = true;
        }

        $countries = Intl::getRegionBundle()->getCountryNames();

        $product_severities = Product::getProductSeverityCodes();

        $mapping = $this->doMapping($product->getValue(), $id);

        /** @var Product $productObj */
        $productObj = $mapping[0];

        if( false == is_null($productObj->getImageKey()) ) {
            $image = $bucket->object($productObj->getImageKey());

            $assetPath = realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR;

            $relativeImagePath = "products/" . $productObj->getImageKey() . '.jpg';
            $storeLocation = "$assetPath/$relativeImagePath";
            if (!file_exists($storeLocation)) {
                $stream = $image->downloadToFile($storeLocation);
            }
        }
        else {
            $relativeImagePath = null;
        }

        $data = array(
            'product' => $productObj,
            'product_image_path' => $relativeImagePath,
            'product_severities' => $product_severities,
            'countries' => $countries,
            'saved' => $saved,
        );


        return $this->render('CMSAdminBundle:Default:edit.html.twig', $data);
    }

    /**
     * @Route("/fbadmin/products/add", name="admin_product_add")
     */
    public function productAddAction(Request $request)
    {
        $reference = 'products';

        $firebase = $this->container->get('firebase');
        /** @var \Kreait\Firebase\Database\Reference $products */
        $products = $firebase->getDatabase()->getReference($reference);

        $product = new Product();

        $saved = false;

        if( $request->isMethod('POST') ) {
            $fields = $request->request;

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $product_image */
            $product_image = $request->files->get('product_image');

            $filename = sha1(time() . $product_image->getClientOriginalName()) . $product_image->getExtension();

            $uploaded = $bucket->upload(
                file_get_contents($product_image->getRealPath()),
                [
                    'name' => sha1(time() . $product_image->getClientOriginalName())
                ]
            );

            $newProduct = new \stdClass();
            $newProduct->title = $fields->get('title');
            $newProduct->website_url = $fields->get('website_url');
            $newProduct->company = $fields->get('company');
            $newProduct->country = $fields->get('country');
            $newProduct->productSeverity = $fields->get('product_severity');
            $newProduct->suggestions = $fields->get('suggestions');
            $newProduct->imageKey = $filename;

//            $update = [
//                "$id" => $newProduct
//            ];
//
//            $products->update($update);

            $key = $products->push(json_encode($newProduct))->getKey();

            $productJson = $products->getChild($key)->getValue();
            $product->loadFromJson($key, $productJson);
            $saved = true;
            return $this->redirect($this->generateUrl('admin_product_edit', array('id' => $key)));
        }

        $countries = Intl::getRegionBundle()->getCountryNames();

        $product_severities = Product::getProductSeverityCodes();

        $data = array(
            'product' => $product,
            'product_severities' => $product_severities,
            'countries' => $countries,
            'saved' => $saved,
        );


        return $this->render('CMSAdminBundle:Default:edit.html.twig', $data);
    }

    protected function doMapping($products, $key = null) {

        $return = array();

        if( !$products ) {
            return $return;
        }

        if( is_null($key) ) {

            foreach ($products as $key => $productJson) {

                $productObj = new Product();
                $productObj->loadFromJson($key, $productJson);
                $return[] = $productObj;
            }
        }
        else {
            $productObj = new Product();
            $productObj->loadFromJson($key, $products);
            $return[] = $productObj;
        }
        return $return;
    }
}
