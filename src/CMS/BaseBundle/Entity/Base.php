<?php

namespace CMS\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Base
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime" , nullable=true)
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    protected $updateAt;

    /**
     * @var string $sale
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    protected $enabled = true;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
   	 * @ORM\JoinColumn(name="createdby", referencedColumnName="id" , nullable=true, onDelete="SET NULL")
   	 **/
    protected $createdby;

	/** @var \Symfony\Component\DependencyInjection\ContainerInterface */
	private $container;

	public function setContainer( \Symfony\Component\DependencyInjection\ContainerInterface $container ) {
		$this->container = $container;
	}

    /**
     * return slug
     *
     * @param string $title
     * @return string slug
     */
    public function returnSlug($title)
    {
        $slug  = self::slugify($title);

        return $slug;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Base
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Base
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Base
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set createdby
     *
     * @param \Application\Sonata\UserBundle\Entity\User $createdby
     *
     * @return Base
     */
    public function setCreatedby(\Application\Sonata\UserBundle\Entity\User $createdby = null)
    {
        $this->createdby = $createdby;

        return $this;
    }

    /**
     * Get createdby
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreatedby()
    {
        return $this->createdby;
    }


    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function __toString() {
        if (method_exists($this, 'setTitle')) {
            return strval( $this->getTitle() );
        }else{
            return "";
        }
   	}

}
