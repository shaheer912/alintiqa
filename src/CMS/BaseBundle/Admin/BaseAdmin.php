<?php

namespace CMS\BaseBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BaseAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
    );

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

    }

    public function getContainer()
    {
        return $this->getConfigurationPool()->getContainer();
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return array(
            'csv'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        // avoid security field to be exported
        return array_filter(parent::getExportFields(), function ($v) {
            $Removedfilterkey = array('id', 'enabled');
            return !in_array($v, $Removedfilterkey);
        });

    }

    public function prePersist($object)
    {

        $now = new \DateTime("now");
        if (method_exists($object, 'setCreateAt')) {
            $object->setCreateAt($now);
        }
        if (method_exists($object, 'setUpdateAt')) {
            $object->setUpdateAt($now);
        }

        if (method_exists($object, 'setCreatedBy')) {
            $object->setCreatedBy($this->getUser());
        }

        if (method_exists($object, 'setEnabled') && method_exists($object, 'getEnabled')) {
            if ($object->getEnabled() == null) {
                $object->setEnabled(false);
            }
        }
    }

    public function preUpdate($object)
    {
        $now = new \DateTime("now");
        if (method_exists($object, 'setUpdateAt')) {
            $object->setUpdateAt($now);
        }
    }

    public function getListModes()
    {
        unset($this->listModes['mosaic']);
        return $this->listModes;
    }

    public function getUser()
    {
        return $this->getSecurityContext()->get('security.token_storage')->getToken()->getUser();
    }

    public function getSecurityContext()
    {
        return $this->getConfigurationPool()->getContainer();
    }
}