-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2018 at 10:49 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alintiqa`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alternate_products`
--

CREATE TABLE `alternate_products` (
  `alternate_product_id` int(11) NOT NULL,
  `original_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `alternate_products`
--

INSERT INTO `alternate_products` (`alternate_product_id`, `original_product_id`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_cities`
--

CREATE TABLE `cms_cities` (
  `id` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `cityname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `citycode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_company`
--

CREATE TABLE `cms_company` (
  `id` int(11) NOT NULL,
  `logo` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registered_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_countries`
--

CREATE TABLE `cms_countries` (
  `id` int(11) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `countryname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countrycode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_countries`
--

INSERT INTO `cms_countries` (`id`, `createdby`, `countryname`, `countrycode`, `create_at`, `update_at`, `enabled`) VALUES
(1, 1, 'Pakistan', 'Pk', '2018-02-05 22:14:35', '2018-02-05 22:14:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_front_user`
--

CREATE TABLE `cms_front_user` (
  `id` int(11) NOT NULL,
  `company` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `industry` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `businessemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `companywebsite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `companyalias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `companysize` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hqaddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locationifdifferentfromhq` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_front_user_group`
--

CREATE TABLE `cms_front_user_group` (
  `front_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_front_user_vendors`
--

CREATE TABLE `cms_front_user_vendors` (
  `front_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_homeslider`
--

CREATE TABLE `cms_homeslider` (
  `id` int(11) NOT NULL,
  `image` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_homeslider`
--

INSERT INTO `cms_homeslider` (`id`, `image`, `createdby`, `title`, `description`, `create_at`, `update_at`, `enabled`) VALUES
(1, 1, NULL, 'Testing', 'Tesitng testing', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_industry`
--

CREATE TABLE `cms_industry` (
  `id` int(11) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_industry`
--

INSERT INTO `cms_industry` (`id`, `createdby`, `title`, `create_at`, `update_at`, `enabled`) VALUES
(1, NULL, 'tete', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_product`
--

CREATE TABLE `cms_product` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suggestions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `product_severity_id` int(11) NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `product_image_id` int(11) DEFAULT NULL,
  `product_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_product`
--

INSERT INTO `cms_product` (`id`, `title`, `country`, `company_name`, `suggestions`, `product_severity_id`, `create_at`, `createdby`, `product_image_id`, `product_website`, `update_at`, `enabled`) VALUES
(1, 'Shampoo', 1, 'Unilever', 'DO NOT USE THIS PRODUCT', 1, '2018-09-02 16:02:59', 1, 2, NULL, '2018-09-02 16:02:59', 1),
(2, 'Toothpaste', 1, 'unilever', 'Toothpaste', 1, '2018-09-02 16:56:30', 1, 2, 'uniliver', '2018-09-02 16:56:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_product_severity`
--

CREATE TABLE `cms_product_severity` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_product_severity`
--

INSERT INTO `cms_product_severity` (`id`, `name`, `code`) VALUES
(1, 'Red Zone', 'redzone');

-- --------------------------------------------------------

--
-- Table structure for table `cms_vendor_category`
--

CREATE TABLE `cms_vendor_category` (
  `id` int(11) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'adminuser', 'adminuser', 'zeeshantraffic@gmail.com', 'zeeshantraffic@gmail.com', 1, NULL, '$2y$13$7S8on7PU.vKqZ/CjuB5oveb/mrA.Tn65cHJrXbLdUkHLYkJYHJh2.', '2018-01-28 23:08:51', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}');

-- --------------------------------------------------------

--
-- Table structure for table `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fos_user_user`
--

CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admin', 'admin', 'zeeshantraffic@gmail.com', 'zeeshantraffic@gmail.com', 1, 'ryDwUqPWEC3eSST7SB1ZlJ4q0S4OSXKB6so5eEhhpOE', 'tNXt3vhlcV5Z0lPMuUMhl8ME8wmMpwgSD2Ra2xZtBY3JfzttF1nBIW4q3HjxUKknPev9BPpczc0gD5w7LQpZaQ==', '2018-09-02 20:52:11', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', '2018-01-31 02:14:12', '2018-09-02 20:52:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media__gallery`
--

CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media__gallery_media`
--

CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media__media`
--

CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media__media`
--

INSERT INTO `media__media` (`id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_identifier`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(1, '1.png', NULL, 0, 'sonata.media.provider.image', 1, 'f2c0c00e5ec90fef5483949c4c60efc2b470430c.png', '{\"filename\":\"1.png\"}', 1600, 4996, NULL, 'image/png', 159045, NULL, NULL, 'default', NULL, NULL, NULL, NULL, '2018-02-05 21:38:32', '2018-02-05 21:38:32'),
(2, 'thumb_1_admin.jpeg', NULL, 0, 'sonata.media.provider.image', 1, 'aa8cea138a9013d0e38d0cf65cadd8c40ffc439c.jpeg', '{\"filename\":\"thumb_1_admin.jpeg\"}', 100, 75, NULL, 'image/jpeg', 5756, NULL, NULL, 'default', NULL, NULL, NULL, NULL, '2018-09-01 03:38:57', '2018-09-01 03:38:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `alternate_products`
--
ALTER TABLE `alternate_products`
  ADD PRIMARY KEY (`alternate_product_id`,`original_product_id`),
  ADD KEY `IDX_BD34EEE5EAD5AD36` (`alternate_product_id`),
  ADD KEY `IDX_BD34EEE5283311CE` (`original_product_id`);

--
-- Indexes for table `cms_cities`
--
ALTER TABLE `cms_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EBABAE135373C966` (`country`),
  ADD KEY `IDX_EBABAE1346D262E0` (`createdby`);

--
-- Indexes for table `cms_company`
--
ALTER TABLE `cms_company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1153065EE48E9A13` (`logo`),
  ADD KEY `IDX_1153065E46D262E0` (`createdby`);

--
-- Indexes for table `cms_countries`
--
ALTER TABLE `cms_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9E008BC046D262E0` (`createdby`);

--
-- Indexes for table `cms_front_user`
--
ALTER TABLE `cms_front_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_868317A04FBF094F` (`company`),
  ADD UNIQUE KEY `UNIQ_868317A02D5B0234` (`city`),
  ADD UNIQUE KEY `UNIQ_868317A0CDFA6CA0` (`industry`),
  ADD KEY `IDX_868317A046D262E0` (`createdby`);

--
-- Indexes for table `cms_front_user_group`
--
ALTER TABLE `cms_front_user_group`
  ADD PRIMARY KEY (`front_user_id`,`group_id`),
  ADD KEY `IDX_36708CC27E5A750F` (`front_user_id`),
  ADD KEY `IDX_36708CC2FE54D947` (`group_id`);

--
-- Indexes for table `cms_front_user_vendors`
--
ALTER TABLE `cms_front_user_vendors`
  ADD PRIMARY KEY (`front_user_id`,`group_id`),
  ADD KEY `IDX_95DB3DE47E5A750F` (`front_user_id`),
  ADD KEY `IDX_95DB3DE4FE54D947` (`group_id`);

--
-- Indexes for table `cms_homeslider`
--
ALTER TABLE `cms_homeslider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7DC57E8EC53D045F` (`image`),
  ADD KEY `IDX_7DC57E8E46D262E0` (`createdby`);

--
-- Indexes for table `cms_industry`
--
ALTER TABLE `cms_industry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A714A05D46D262E0` (`createdby`);

--
-- Indexes for table `cms_product`
--
ALTER TABLE `cms_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8DA60BBC5373C966` (`country`),
  ADD KEY `IDX_8DA60BBC5B7DEA1E` (`product_severity_id`),
  ADD KEY `IDX_8DA60BBCF6154FFA` (`product_image_id`),
  ADD KEY `IDX_8DA60BBC46D262E0` (`createdby`);

--
-- Indexes for table `cms_product_severity`
--
ALTER TABLE `cms_product_severity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_vendor_category`
--
ALTER TABLE `cms_vendor_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7BA3F93F46D262E0` (`createdby`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indexes for table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`);

--
-- Indexes for table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_C560D761C05FB297` (`confirmation_token`);

--
-- Indexes for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indexes for table `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Indexes for table `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_cities`
--
ALTER TABLE `cms_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_company`
--
ALTER TABLE `cms_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_countries`
--
ALTER TABLE `cms_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_front_user`
--
ALTER TABLE `cms_front_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_homeslider`
--
ALTER TABLE `cms_homeslider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_industry`
--
ALTER TABLE `cms_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_product`
--
ALTER TABLE `cms_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_product_severity`
--
ALTER TABLE `cms_product_severity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_vendor_category`
--
ALTER TABLE `cms_vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fos_user_group`
--
ALTER TABLE `fos_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fos_user_user`
--
ALTER TABLE `fos_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Constraints for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `alternate_products`
--
ALTER TABLE `alternate_products`
  ADD CONSTRAINT `FK_BD34EEE5283311CE` FOREIGN KEY (`original_product_id`) REFERENCES `cms_product` (`id`),
  ADD CONSTRAINT `FK_BD34EEE5EAD5AD36` FOREIGN KEY (`alternate_product_id`) REFERENCES `cms_product` (`id`);

--
-- Constraints for table `cms_cities`
--
ALTER TABLE `cms_cities`
  ADD CONSTRAINT `FK_EBABAE1346D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_EBABAE135373C966` FOREIGN KEY (`country`) REFERENCES `cms_countries` (`id`);

--
-- Constraints for table `cms_company`
--
ALTER TABLE `cms_company`
  ADD CONSTRAINT `FK_1153065E46D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_1153065EE48E9A13` FOREIGN KEY (`logo`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `cms_countries`
--
ALTER TABLE `cms_countries`
  ADD CONSTRAINT `FK_9E008BC046D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cms_front_user`
--
ALTER TABLE `cms_front_user`
  ADD CONSTRAINT `FK_868317A02D5B0234` FOREIGN KEY (`city`) REFERENCES `cms_cities` (`id`),
  ADD CONSTRAINT `FK_868317A046D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_868317A04FBF094F` FOREIGN KEY (`company`) REFERENCES `cms_company` (`id`),
  ADD CONSTRAINT `FK_868317A0CDFA6CA0` FOREIGN KEY (`industry`) REFERENCES `cms_industry` (`id`);

--
-- Constraints for table `cms_front_user_group`
--
ALTER TABLE `cms_front_user_group`
  ADD CONSTRAINT `FK_36708CC27E5A750F` FOREIGN KEY (`front_user_id`) REFERENCES `cms_front_user` (`id`),
  ADD CONSTRAINT `FK_36708CC2FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`);

--
-- Constraints for table `cms_front_user_vendors`
--
ALTER TABLE `cms_front_user_vendors`
  ADD CONSTRAINT `FK_95DB3DE47E5A750F` FOREIGN KEY (`front_user_id`) REFERENCES `cms_front_user` (`id`),
  ADD CONSTRAINT `FK_95DB3DE4FE54D947` FOREIGN KEY (`group_id`) REFERENCES `cms_vendor_category` (`id`);

--
-- Constraints for table `cms_homeslider`
--
ALTER TABLE `cms_homeslider`
  ADD CONSTRAINT `FK_7DC57E8E46D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_7DC57E8EC53D045F` FOREIGN KEY (`image`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `cms_industry`
--
ALTER TABLE `cms_industry`
  ADD CONSTRAINT `FK_A714A05D46D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cms_product`
--
ALTER TABLE `cms_product`
  ADD CONSTRAINT `FK_8DA60BBC46D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_8DA60BBC5373C966` FOREIGN KEY (`country`) REFERENCES `cms_countries` (`id`),
  ADD CONSTRAINT `FK_8DA60BBC5B7DEA1E` FOREIGN KEY (`product_severity_id`) REFERENCES `cms_product_severity` (`id`),
  ADD CONSTRAINT `FK_8DA60BBCF6154FFA` FOREIGN KEY (`product_image_id`) REFERENCES `media__media` (`id`);

--
-- Constraints for table `cms_vendor_category`
--
ALTER TABLE `cms_vendor_category`
  ADD CONSTRAINT `FK_7BA3F93F46D262E0` FOREIGN KEY (`createdby`) REFERENCES `fos_user_user` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
